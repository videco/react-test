import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import hh from 'react-hyperscript-helpers'
import { memoizeWith, apply, join } from 'ramda'

const tap = x => {console.log(x); return x}

const maybeUpper = memoizeWith(
  x => x,
  (counter, saludo) =>  
    hh.div(counter > 10 
      ? tap('saludo', saludo).toUpperCase() 
      : tap(saludo))
)

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      saludo: 'Diego',
      counter: 0,
      upper: false
    }
  }

  countUp() {    
    this.setState(({saludo, counter}) => ({saludo, counter: counter+1}))
  }

  namey() {
    return this.state.counter + ' Diego'
  }

  maybeUpper() {
    return this.state.counter > 10 ? this.state.saludo.toUpperCase() : this.state.saludo
  }

  doThing() {
    console.log('doing thing')
    this.setState(({saludo, counter}) => ({thing: (this.state.thing || 0)+1}))    
  }

  render() {
    let {div, header, img, h1, button} = hh
    return (
      div(".App", [
        header(".App-header", [
          img({src: logo,  className: "App-logo"}),
          h1(".App-title", 
            [`Welcome to Hola`, maybeUpper(this.state.counter, this.namey()),  this.state.counter]
          ),
          button({onClick: () => this.countUp()}, 'Count up'),
          button({onClick: () => this.doThing()}, 'Do Thing '),
        ])
      ])
    );
  }
}

export default App;
